//
//  LoginController.m
//  cook
//
//  Created by chenwenpeng on 14-1-19.
//  Copyright (c) 2014年 cook. All rights reserved.
//

#import "LoginController.h"

@interface LoginController (){
    
    UITextField *_username;
    UITextField *_password;
    UIButton *_login;
}
@end

@implementation LoginController

@synthesize flag;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithRed:243/255.0 green:243/255.0 blue:243/255.0 alpha:1];
}

- (void)viewWillAppear:(BOOL) animated{
    [self dropData];
    [self createDB];
    NSMutableDictionary *contact = [self query];
    NSString *user_id = [contact objectForKey:@"userId"];
    //初始化
    [self removeViews];
    if(contact && [user_id isEqualToString:NO_USER]){
        //没有登录
        [self createLoginView];
    }else {
        //已经登录
        [self createMyCenterView];
        
        //[self createLoginView];
    }
}


//转到注册
-(void) toRegist:(id *) sender{
    
    RegistController *registController = [[RegistController alloc] init];
    [self presentModalViewController:registController animated:YES];
    
    [registController release];
}

//验证登录
-(void) login:(id *) sender{
    
    NSString *username = _username.text;
    NSString *password = _password.text;
    
    NSString *url = [NSString stringWithFormat:@"%@%@%@%@",@"http://www.0707wm.com/android/login?username=",username,@"&password=",password];
    
    [HttpPostExecutor postExecuteWithUrlStr: url
                                  Paramters:@""
                        FinishCallbackBlock:^(NSString *result){
                            // 执行post请求完成后的逻辑
                        NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData: [result dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
                            
                            NSString *where = [resultDic objectForKey:@"flag"];
                            //成功
                            if([SUCCESS_LOGIN isEqualToString:where]){
                                NSString *userid = [NSString stringWithFormat:@"%@",[resultDic objectForKey:@"id"]];
                                //存库
                                [self update:userid userName:username];
                                //默认转到个人中心
                                [self removeViews];
                                [self createMyCenterView];
                                
                            }else {
                                [self.view makeToast:@"用户名或密码错误" duration:1.0 position:@"center"];
                            }
                            
                        } ErrorCallbackBlock:^(NSString *error){
                            
                            [self.view makeToast:@"网络不给力" duration:1.0 position:@"center"];
                        }];
}


-(void) removeViews{
    
    if(self.flag == nil || [self.flag isEqualToString:TO_MYCENTER]){
        
        for(UIView *view in [self.view subviews])
        {
            [view removeFromSuperview];
        }
    }
}

//转到注册
-(void) logout:(id *) sender{
    
    [self removeViews];
    [self update:NO_USER userName:@""];
    [self createLoginView];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) createLoginView{
    //导航
    UILabel *titleView = [[[NSBundle mainBundle] loadNibNamed:@"TitleView" owner:self options:nil] objectAtIndex:0];
    titleView.frame = CGRectMake(70, 0, 180, 40);
    [titleView setTextAlignment:NSTextAlignmentCenter];
    [titleView setText:@"登录"];
    
    UIButton *left = [[[NSBundle mainBundle] loadNibNamed:@"TitleTextView" owner:self options:nil] objectAtIndex:0];
    [left setFrame:CGRectMake(0, 0, 70, 40)];
    [self.view addSubview:left];
    
    UIButton *right = [[[NSBundle mainBundle] loadNibNamed:@"TitleTextView" owner:self options:nil] objectAtIndex:0];
    right.frame = CGRectMake(250, 0, 70, 40);
    [right setTitle:@"注册" forState:UIControlStateNormal];
    
    [self.view addSubview:right];
    [right addTarget: self action:@selector(toRegist:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:titleView];
    
    
    //用户名
    _username = [[UITextField alloc] initWithFrame:CGRectMake(20, 80, 280, 30)];
    _username.clearButtonMode = UITextFieldViewModeAlways;
    _username.backgroundColor = [UIColor whiteColor];
    _username.placeholder = @"请输入用户名";
    [_username setTag:TAG_USERNAME];
    _username.autocapitalizationType = UITextAutocapitalizationTypeNone;
    [_username setDelegate:self];
    [self.view addSubview:_username];
    
    
    _password = [[UITextField alloc] initWithFrame:CGRectMake(20, 120, 280, 30)];
    _password.clearButtonMode = UITextFieldViewModeAlways;
    //password.secureTextEntry = YES;
    _password.backgroundColor = [UIColor whiteColor];
    _password.placeholder = @"请输入密码";
    _password.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _password.returnKeyType = UIReturnKeyGo;
    [_password setTag:TAG_PASSWORD];
    [_password setDelegate:self];
    [self.view addSubview:_password];
    
    _login = [[UIButton alloc] init];
    _login.frame = CGRectMake(20, 170, 280, 30);
    _login.backgroundColor = [UIColor orangeColor];
    [_login setTitle:@"登 陆" forState:UIControlStateNormal];
    [_login addTarget:self action:@selector(login:) forControlEvents:UIControlEventTouchUpInside];
    
    [_login setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:_login];
    
    
    [_username release];
    [_password release];
    [_login release];
}


-(void) createMyCenterView{
    
    UILabel *titleView = [[[NSBundle mainBundle] loadNibNamed:@"TitleView" owner:self options:nil] objectAtIndex:0];
    titleView.frame = CGRectMake(70, 0, 180, 40);
    [titleView setTextAlignment:NSTextAlignmentCenter];
    [titleView setText:@"个人中心"];
    [self.view addSubview:titleView];
    
    UIButton *left = [[[NSBundle mainBundle] loadNibNamed:@"TitleTextView" owner:self options:nil] objectAtIndex:0];
    [left setFrame:CGRectMake(0, 0, 70, 40)];
    [self.view addSubview:left];
    
    UIButton *right = [[[NSBundle mainBundle] loadNibNamed:@"TitleTextView" owner:self options:nil] objectAtIndex:0];
    right.frame = CGRectMake(250, 0, 70, 40);
    [right setTitle:@"退出" forState:UIControlStateNormal];
    [right addTarget: self action:@selector(logout:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:right];

    UserCenterView *userCenter = [[[NSBundle mainBundle] loadNibNamed:@"UserCenter" owner:self options:nil] objectAtIndex:0];
    userCenter.frame =CGRectMake(0, 40, 320, 800);
    [self.view addSubview:userCenter];
    
    
    //库里面更新本地view
    NSDictionary *userInfo = [self query];
    
    NSString *usernameInDB = [userInfo objectForKey:@"userName"];
    NSString *userPhoneInDB = [userInfo objectForKey:@"phone"];
    NSString *userAddressInDB = [userInfo objectForKey:@"address"];
    
    UILabel *usernameView = (UILabel *)[userCenter viewWithTag:TAG_XIB_USERNAME];
    UILabel *userPhoneView = (UILabel *)[userCenter viewWithTag:TAG_XIB_PHONE];
    UILabel *userAddressView = (UILabel *)[userCenter viewWithTag:TAG_XIB_ADDRESS];
    
    usernameView.text = usernameInDB == nil|| [usernameInDB isEqualToString:@""]? @"" : usernameInDB;
    userPhoneView.text = userPhoneInDB == nil || [userPhoneInDB isEqualToString:@""] ? @"暂无" : userPhoneInDB;
    userAddressView.text = userAddressInDB == nil || [userAddressInDB isEqualToString:@""] ? @"暂无" : userAddressInDB;
    
    
    UIButton *logoutBT =(UIButton *)[userCenter viewWithTag: TAG_XIB_LOGOUT];
    
    [logoutBT addTarget:self action:@selector(logout:) forControlEvents:UIControlEventTouchUpInside];

    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    //官方 取消第一响应者（就是退出编辑模式收键盘）
    if(textField.tag == TAG_USERNAME){
        [textField resignFirstResponder];
    }else {
        
        [self login:nil];
    }
    return YES;
}

@end
