//
//  RestaurantsViewController.m
//  cook
//
//  Created by chenwenpeng on 13-12-29.
//  Copyright (c) 2013年 cook. All rights reserved.
//

#import "RestaurantsViewController.h"
#import "MenuViewController.h"
#import "AddressViewController.h"
#import "CommonViewController.h"
#import "HttpPostExecutor.h"
#import "Toast+UIView.h"
#import "RestView.h"
#import "Utils.h"
#import "Constants.h"

@interface RestaurantsViewController ()<UITableViewDataSource,UITableViewDelegate>{

    @private
    UITableView *_dataTable;
    @private
    NSMutableArray *_dataList;
}

@end

@implementation RestaurantsViewController

@synthesize buildingId,buildingName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
       
    }
    return self;
}

-(void) toAddress:(id) sender{
    
    AddressViewController *address = [[AddressViewController alloc] init];
    
    [self presentModalViewController:address animated:YES];
    [self setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [address release];
}

-(void) dealloc{
    [super dealloc];
    [_dataList release];
    [_dataTable release];
}


-(void) fetchFromLocalDB{
    
    [self createDB];
    NSMutableDictionary *buildingInDB = [self query];
    if([[buildingInDB objectForKey:@"buildingId"] isEqualToString:NO_BUILDING]){
        //无数据
        [self toAddress:nil];
    }else {
        self.buildingId =[buildingInDB objectForKey:@"buildingId"];
        self.buildingName = [buildingInDB objectForKey:@"buildingName"];
    }
}

-(void) fetchDataFromServer{
    
    NSString *url = [NSString stringWithFormat:@"%@%@",@"http://www.0707wm.com/android/getRestaurants?buildingId=",self.buildingId];
    
    [HttpPostExecutor postExecuteWithUrlStr: url
                                  Paramters:@""
                        FinishCallbackBlock:^(NSString *result){
                            if(result == nil || [result isEqualToString:@""]){
                                result = @"";
                            }
                            // 执行post请求完成后的逻辑
                            NSMutableArray *dataList = [NSJSONSerialization JSONObjectWithData: [result dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
                            //set一遍才能起作用
                            [_dataList removeAllObjects];
                            [_dataList addObjectsFromArray:dataList];
                            [_dataTable reloadData];
                        } ErrorCallbackBlock:^(NSString *error){
                            
                            [self.view makeToast:@"网络不给力" duration:1.0 position:@"center"];
                        }];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController setNavigationBarHidden:YES];
    
    UILabel *titleView = [[[NSBundle mainBundle] loadNibNamed:@"TitleView" owner:self options:nil] objectAtIndex:0];
    titleView.frame = CGRectMake(70, 0, 180, 40);
    [titleView setTextAlignment:NSTextAlignmentCenter];
    [titleView setText:@"周边外卖"];
    
    [self.view addSubview:titleView];
    
    UIButton *left = [[[NSBundle mainBundle] loadNibNamed:@"TitleTextView" owner:self options:nil] objectAtIndex:0];
    [left setFrame:CGRectMake(0, 0, 70, 40)];
    [left setTitle:@"选择地址" forState:UIControlStateNormal];
    [left setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    [left setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [left addTarget: self action:@selector(toAddress:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:left];
    
    
    UIButton *right = [[[NSBundle mainBundle] loadNibNamed:@"TitleImageView" owner:self options:nil] objectAtIndex:0];
    right.frame = CGRectMake(250, 0, 70, 40);
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(30, 10, 25, 22)];
    [image setImage:[UIImage imageNamed:@"rightRefresh.png"]];
    [right addSubview:image];
    [self.view addSubview:right];
    [right addTarget: self action:@selector(fetchDataFromServer) forControlEvents:UIControlEventTouchUpInside];
    
    
    _dataTable =[[UITableView alloc] initWithFrame:CGRectMake(0, 65, self.view.frame.size.width, self.view.frame.size.height-65)];
    [_dataTable setDelegate:self];
    [_dataTable setDataSource:self];
    [self.view addSubview:_dataTable];
    
    [self.view addSubview:_dataTable];
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)viewWillAppear:(BOOL) animated{
    //从本地数据库获取数据
    NSLog(@"从本地数据库获取数据");
    [self fetchFromLocalDB];
    
    _dataList = [[NSMutableArray alloc] init];
    //获取数据
    [self fetchDataFromServer];
    
    UILabel *centerBar = [[[NSBundle mainBundle] loadNibNamed:@"RestsCenterBar" owner:self options:nil] objectAtIndex:0];
    centerBar.frame = CGRectMake(0, 40, 320, 25);
    [centerBar setText: [NSString stringWithFormat:@"%@%@",@"  您的位置是：",self.buildingName]];
    [self.view addSubview:centerBar];

}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_dataList count];
}

- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (RestView *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RestView *rest = [[[NSBundle mainBundle] loadNibNamed:@"RestView" owner:self options:nil] objectAtIndex:0];
    
    NSDictionary *restData = [_dataList objectAtIndex:indexPath.section];
    
    
    NSString *restName = [restData objectForKey:@"name"];
    NSString *notice = [restData objectForKey:@"notice"];
    NSString *deliveryTime = [NSString stringWithFormat:@"%@",[restData objectForKey:@"deliveryTime"]];
    NSString *deliveryValue = [NSString stringWithFormat:@"%@",[restData objectForKey:@"deliveryValue"]];
    NSString *scoreStr = [restData objectForKey:@"score"];
    NSString *isCloseStr =[restData objectForKey:@"isClosed"];
    NSInteger score =[Utils parseInteger:scoreStr];
    NSInteger isClosed = [Utils parseInteger:isCloseStr];//1 营业中 2打烊
    
    [rest.name setText:restName];
    [rest.notice setText:notice];
    [rest.deliveryTime setText:deliveryTime];
    [rest.deliveryPrice setText:deliveryValue];
    rest.frame = CGRectMake(0, 0, 320, 70);
    if(isClosed == kREST_OPEN){
        rest.backgroundColor = [UIColor whiteColor];
        [rest.open setHidden:YES];
    }else {
        rest.backgroundColor = [UIColor colorWithRed:243/255.0 green:243/255.0 blue:243/255.0 alpha:1];
    }
    
    UIImage *star_light = [UIImage imageNamed:@"star_3_1.png"];
    if(score>kREST_MINAVGSCORE && score <=4) {
        [rest.img_3 setImage:star_light];
    }else if(score>kREST_MINAVGSCORE && score <=5){
        [rest.img_3 setImage:star_light];
        [rest.img_4 setImage:star_light];
    }
    return rest;
}

//选择了餐厅以后
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MenuViewController *menuViewController = [[MenuViewController alloc]init];
    
    NSString *restId = [[_dataList objectAtIndex:indexPath.section] objectForKey:@"id"];
    NSString *restName = [[_dataList objectAtIndex:indexPath.section] objectForKey:@"name"];
    menuViewController.buildingId = self.buildingId;
    menuViewController.restId = restId;
    menuViewController.restName = restName;
    
    [self.navigationController pushViewController:menuViewController animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [menuViewController release];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
