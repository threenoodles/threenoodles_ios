//
//  RestView.h
//  cook
//
//  Created by chenwenpeng on 14-2-20.
//  Copyright (c) 2014年 cook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestView : UITableViewCell

@property(nonatomic,retain) IBOutlet UILabel *name;
@property(nonatomic,retain) IBOutlet UILabel *notice;
@property(nonatomic,retain) IBOutlet UIImageView *img_0;
@property(nonatomic,retain) IBOutlet UIImageView *img_1;
@property(nonatomic,retain) IBOutlet UIImageView *img_2;
@property(nonatomic,retain) IBOutlet UIImageView *img_3;
@property(nonatomic,retain) IBOutlet UIImageView *img_4;

@property(nonatomic,retain) IBOutlet UILabel *deliveryTime;
@property(nonatomic,retain) IBOutlet UILabel *deliveryPrice;

@property(nonatomic,retain) IBOutlet UILabel *open;
@end
