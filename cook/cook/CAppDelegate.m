//
//  CAppDelegate.m
//  cook
//
//  Created by chenwenpeng on 13-12-28.
//  Copyright (c) 2013年 cook. All rights reserved.
//

#import "CAppDelegate.h"
#import "AddressViewController.h"
#import "LoginController.h"
#import "RegistController.h"
#import "AboutUsController.h"
#import "RestaurantsViewController.h"
#import "LoginController.h"

@implementation CAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
//    AddressViewController *addressViewController = [[AddressViewController alloc] init];
//    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:addressViewController];
//    addressViewController.title = @"切换地址";
//    [navigationController setNavigationBarHidden:YES];
    
//    LoginController *loginController = [[LoginController alloc] init];
    
    //self.window.rootViewController = navigationController;
    //self.window.rootViewController = loginController;
    
//    RegistController *registView = [[RegistController alloc] init];
    //self.window.rootViewController = registView;
    
//    AboutUsController *aboutUsController = [[AboutUsController alloc] init];
//    //导航
//    UIView *titleView = [[[NSBundle mainBundle] loadNibNamed:@"RestsTopNavi" owner:self options:nil] objectAtIndex:0];
//    titleView.frame = CGRectMake(0, 0, 320, 40);
//    [aboutUsController.view addSubview:titleView];
    //self.window.rootViewController = aboutUsController;
    //    [loginController release];
    //    [navigationController release];
    //    [registView release];
    //    [addressViewController release];
    //    [aboutUsController release];
    //    

    
    
    
    RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc] init];
    restaurantsViewController.title = @"店铺";
    restaurantsViewController.tabBarItem.image = [UIImage imageNamed:@"baritem1.png"];
    UINavigationController *restaurantNaviViewController = [[UINavigationController alloc] initWithRootViewController:restaurantsViewController];
    
    
    
    UIViewController *orderController = [[UIViewController alloc] init];
    orderController.title = @"我的订单";
    orderController.tabBarItem.image = [UIImage imageNamed:@"baritem4.png"];
    
    
    
    LoginController *mycenterController = [[LoginController alloc] init];
    mycenterController.title = @"个人中心";
    mycenterController.tabBarItem.image = [UIImage imageNamed:@"baritem2.png"];
    
    
    AboutUsController *moreController = [[AboutUsController alloc] init];
    moreController.title = @"更多";
    moreController.tabBarItem.image = [UIImage imageNamed:@"baritem3.png"];
    
    NSArray *controllers = [NSArray arrayWithObjects:restaurantNaviViewController,orderController,mycenterController,moreController,nil];
    UITabBarController *tbController = [[UITabBarController alloc] init];
    tbController.viewControllers = controllers;
    
    self.window.rootViewController =tbController;
    
    
    
    [mycenterController release];
    [orderController release];
    [moreController release];
    [restaurantsViewController release];
    [tbController release];
    [restaurantNaviViewController release];
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
