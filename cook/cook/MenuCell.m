//
//  MenuCell.m
//  cook
//
//  Created by chenwenpeng on 14-1-12.
//  Copyright (c) 2014年 cook. All rights reserved.
//

#import "MenuCell.h"

@implementation MenuCell

@synthesize name,price,count;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void) dealloc{

    self.name = nil;
    self.price = nil;
    self.count = nil;
    [super dealloc];
}
@end
