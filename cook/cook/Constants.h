//
//  Constants.h
//  cook
//
//  Created by chenwenpeng on 14-2-22.
//  Copyright (c) 2014年 cook. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kREST_OPEN 1
#define kREST_CLOSED 2
#define kREST_MINAVGSCORE 3

@interface Constants : NSObject{
}
@end
