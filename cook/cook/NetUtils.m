//
//  NetUtils.m
//  cook
//
//  Created by chenwenpeng on 14-2-9.
//  Copyright (c) 2014年 cook. All rights reserved.
//

#import "NetUtils.h"
#import "Reachability.h"
@implementation NetUtils
//////////////////////////////////////////////////////
// Network Type Check
//////////////////////////////////////////////////////

+(NSString*)currentNetworkType
{
    NSString* result;
    
    Reachability *r = [Reachability reachabilityWithHostName:@"www.apple.com"];
    
    switch ([r currentReachabilityStatus])
    {
        case NotReachable:    // 没有网络连接
        {
            result = nil;
        }
            break;
            
        case ReachableViaWWAN:// 3G
        {
            result = @"3G";
        }
            break;
            
        case ReachableViaWiFi:// WiFi
        {
            result = @"WIFI";
        }
            break;
    }
    return result;
}
@end
