#import "MenuViewController.h"
#import "MenuCell.h"
#import "MenuUpdater.h"
#import "JSBadgeView.h"
#import "UIPopoverListView.h"
#import "MenuCell_list.h"
#import "HttpPostExecutor.h"
#import "Toast+UIView.h"
#import "Utils.h"

#define NUMBER_OF_VISIBLE_ITEMS 25
#define ITEM_SPACING 110.0f
#define INCLUDE_PLACEHOLDERS YES
@interface MenuViewController ()<UITableViewDataSource,UITableViewDelegate,UIPopoverListViewDataSource, UIPopoverListViewDelegate>{
@private
    UITableView *_dataTable;
@private
    NSMutableArray *_dataList;
@private
    NSMutableArray *_dataAllList;
@private
    NSMutableArray *_categoryList;
@private
    UIPopoverListView *_poplistview;
@private
    NSMutableArray *_orderList;
    
}

@property (nonatomic, assign) BOOL wrap;
@property (nonatomic, assign) NSInteger _menuCount;
@property (assign)BOOL isOpen;
@property (nonatomic,retain)NSIndexPath *selectIndex;
@property (nonatomic,assign) double _deliverPrice;

@end

@implementation MenuViewController

@synthesize carousel;
@synthesize homeTopSVBack;
@synthesize wrap;
@synthesize isOpen,selectIndex;
@synthesize allCount;
@synthesize cart;
@synthesize _menuCount;
@synthesize _deliverPrice;
@synthesize buildingId;
@synthesize restId;
@synthesize restName;

-(void) toRestaurants:(id) sender{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}
//记载cate 对应menu
- (void)fetchMenuListFromServer:(NSString *) cateId atCateIndex:(NSInteger) cateIndex{
    
    NSString *url = [NSString stringWithFormat:@"%@%@%@%@",@"http://www.0707wm.com/android/getCategoyr_menuList?restId=",self.restId,@"&cateId=",cateId];
    [HttpPostExecutor postExecuteWithUrlStr:url
                                  Paramters:@""
                        FinishCallbackBlock:^(NSString *result){
                            // 执行post请求完成后的逻辑
                            NSArray *menuList = [NSJSONSerialization JSONObjectWithData: [result dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
                           
                            //[_dataList addObjectsFromArray:menuList];
                            //[[_dataAllList objectAtIndex:cateIndex] addObjectsFromArray:menuList];
                            [self menuDataInit:menuList container:[_dataAllList objectAtIndex:cateIndex]];
                            
                            [_dataTable reloadData];
                        } ErrorCallbackBlock:^(NSString *error){
                            [self.view makeToast:@"网络不给力" duration:1.0 position:@"center"];
                        }];
}

//初始化数据
- (void)fetchInitDataFromServer{
    NSString * url = [NSString stringWithFormat:@"%@%@%@%@",@"http://www.0707wm.com/android/getRestInfo?buildingId=",self.buildingId,@"&restId=",self.restId];
    [HttpPostExecutor postExecuteWithUrlStr:url
                                  Paramters:@""
                        FinishCallbackBlock:^(NSString *result){
                            if(result == nil || [result isEqualToString:@""]){
                                result = @"";
                            }
                            // 执行post请求完成后的逻辑
                            NSDictionary *restInfo = [NSJSONSerialization JSONObjectWithData: [result dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
                            //price
                            NSString *deliPriceStr = [NSString stringWithFormat:@"%@",[restInfo objectForKey:@"deliPrice"]];
                            double deliPrice = [Utils parseDouble:deliPriceStr];
                            self._deliverPrice = deliPrice;
                            //菜品大类列表
                            NSArray *cateList =[restInfo objectForKey:@"categoryList"];
                            [_categoryList removeAllObjects];
                            [_categoryList addObjectsFromArray:cateList];
                            if([_categoryList count] >0){
                                NSDictionary *cate = [_categoryList objectAtIndex:0];
                                NSDictionary *_cate = [[NSDictionary alloc] initWithObjectsAndKeys:@"0",@"id",@"店长推荐",@"name", nil];
                                NSArray *rList = [cate objectForKey:@"menuList"];
                                [_dataList removeAllObjects];
                                [_dataList addObjectsFromArray: rList];
                                [_categoryList setObject:_cate atIndexedSubscript:0];
                                [_cate release];
                            }
                            
                            //初始化
                            for(int i = 0;i<[_categoryList count];i++){
                                NSMutableArray *menuList =[[NSMutableArray alloc]init];
                                if(i == 0){
                                    
                                    [self menuDataInit:_dataList container:menuList];
                                }
                                [_dataAllList addObject:menuList];
                                [menuList release];
                            }
                            
                            [carousel reloadData];
                            [_dataTable reloadData];
                        } ErrorCallbackBlock:^(NSString *error){
                            [self.view makeToast:@"网络不给力" duration:1.0 position:@"center"];
                        }];
}

//给menu 设置值
-(void) menuDataInit:(NSArray *) menuList container:(NSMutableArray *) containerList{
    
    for (NSDictionary *_menu in menuList) {
        NSNumber *id = [_menu objectForKey:@"id"];
        NSNumber *price = [_menu objectForKey:@"price"];
        NSString *name = [_menu objectForKey:@"name"];
        
        NSMutableDictionary *menu =[[NSMutableDictionary alloc] init];
        [menu setValue:id forKey:@"id"];
        [menu setValue:price forKey:@"price"];
        [menu setValue:name forKey:@"name"];
        [menu setValue:0 forKey:@"count"];
        
        [containerList addObject:menu];
        [menu release];
    }

}

//初始化监听器
-(void) setUpListener{

    [self.cart addTarget:self action:@selector(initPopList:) forControlEvents:UIControlEventTouchUpInside];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]))
    {
        _categoryList =[[NSMutableArray alloc] init];
        _dataAllList = [[NSMutableArray alloc] init];
        _dataList = [[NSMutableArray alloc] init];
        _orderList = [[NSMutableArray alloc] init];
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setUpListener];
    
    //菜品大类
    [self fetchInitDataFromServer];
    
    UILabel *titleView = [[[NSBundle mainBundle] loadNibNamed:@"TitleView" owner:self options:nil] objectAtIndex:0];
    titleView.frame = CGRectMake(70, 0, 180, 40);
    [titleView setTextAlignment:NSTextAlignmentCenter];
    [titleView setText:self.restName];
    
    UIButton *left = [[[NSBundle mainBundle] loadNibNamed:@"TitleTextView" owner:self options:nil] objectAtIndex:0];
    [left setFrame:CGRectMake(0, 0, 70, 40)];
    [left setTitle:@"返回" forState:UIControlStateNormal];
    [left setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    [left setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [left addTarget: self action:@selector(toRestaurants:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:left];
    
    UIButton *right = [[[NSBundle mainBundle] loadNibNamed:@"TitleImageView" owner:self options:nil] objectAtIndex:0];
    right.frame = CGRectMake(250, 0, 70, 40);
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(30, 10, 25, 20)];
    [image setImage:[UIImage imageNamed:@"rightRefresh.png"]];
    [right addSubview:image];
    [self.view addSubview:right];
    [right addTarget: self action:@selector(fetchInitDataFromServer) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:titleView];
    
    
    self.isOpen = NO;
    //默认第一个可见
    //[_dataAllList setObject:_dataList atIndexedSubscript:0];
    [carousel setBackgroundColor:[UIColor clearColor]];
    [homeTopSVBack setImage:[UIImage imageNamed:@"topBar.png"]];
    //
    _dataTable =[[UITableView alloc] initWithFrame:CGRectMake(0, 80, self.view.frame.size.width, self.view.frame.size.height-130)];
    [_dataTable setDelegate:self];
    [_dataTable setDataSource:self];
    [self.view addSubview:_dataTable];
    carousel.decelerationRate = 0.5;
    carousel.type = iCarouselTypeLinear;
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)viewDidUnload
{
    [self setHomeTopSVBack:nil];
    [self setCarousel:nil];
    [self setAllCount:nil];
    [self setCart:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)dealloc {
    
    [_dataAllList release];
    [_dataTable release];
    [homeTopSVBack release];
    [carousel release];
    [allCount release];
    [_categoryList release];
    [cart release];
    [super dealloc];
}




#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
     NSLog(@"--numberOfItemsInCarousel----");
    return [_categoryList count];
}

- (NSUInteger)numberOfVisibleItemsInCarousel:(iCarousel *)carousel
{
     NSLog(@"--numberOfVisibleItemsInCarousel----");
    //limit the number of items views loaded concurrently (for performance reasons)
    //this also affects the appearance of circular-type carousels
    return NUMBER_OF_VISIBLE_ITEMS;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
            NSString *cateName = @"店长推荐";
            if([_categoryList count] >0){
                NSDictionary *cate = [_categoryList objectAtIndex:index];
                cateName = [cate objectForKey:@"name"];
            }
    
            view = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 101,40)];
            [view setBackgroundColor:[UIColor clearColor]];
            UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0,0,101,40)];
            label.textColor = [UIColor whiteColor];
            label.textAlignment = UITextAlignmentCenter;
            label.text = cateName;
            label.font= [UIFont fontWithName:@"Helvetica-Bold" size:14];
    
            label.backgroundColor = [UIColor clearColor];
            [view addSubview:label];
            [label release];

	return view;
}

- (NSUInteger)numberOfPlaceholdersInCarousel:(iCarousel *)carousel
{
    NSLog(@"--numberOfPlaceholdersInCarousel----");
	//note: placeholder views are only displayed on some carousels if wrapping is disabled
	return INCLUDE_PLACEHOLDERS? 2: 0;
}

- (UIView *)carousel:(iCarousel *)carousel placeholderViewAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
      NSLog(@"--placeholderViewAtIndex----");
	UILabel *label = nil;
	
	//create new view if no view is available for recycling
	if (view == nil)
	{
		view = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"page.png"]] autorelease];
		label = [[[UILabel alloc] initWithFrame:view.bounds] autorelease];
		label.backgroundColor = [UIColor clearColor];
		label.textAlignment = UITextAlignmentCenter;
		label.font = [label.font fontWithSize:50.0f];
		[view addSubview:label];
	}
	else
	{
		label = [[view subviews] lastObject];
	}
	
    //set label
	label.text = (index == 0)? @"[": @"]";
	
	return view;
}

- (CGFloat)carouselItemWidth:(iCarousel *)carousel
{
     NSLog(@"--carouselItemWidth----");
    //usually this should be slightly wider than the item views
    return ITEM_SPACING;
}

- (CGFloat)carousel:(iCarousel *)carousel itemAlphaForOffset:(CGFloat)offset
{
     NSLog(@"--itemAlphaForOffset----");
	//set opacity based on distance from camera
    return 1.0f - fminf(fmaxf(offset, 0.0f), 1.0f);
}

- (CATransform3D)carousel:(iCarousel *)_carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform
{
     NSLog(@"--itemTransformForOffset----");
    //implement 'flip3D' style carousel
    transform = CATransform3DRotate(transform, M_PI / 8.0f, 0.0f, 1.0f, 0.0f);
    return CATransform3DTranslate(transform, 0.0f, 0.0f, offset * carousel.itemWidth);
}

- (BOOL)carouselShouldWrap:(iCarousel *)carousel
{
    NSLog(@"--carouselShouldWrap----");
    return NO;
}
//当滑动停止时
- (void)carouselDidEndScrollingAnimation:(iCarousel *)_carousel{
    
        NSInteger index = _carousel.currentItemIndex;
        if([_dataAllList count] > index){
            
            _dataList = [_dataAllList objectAtIndex:index];
            if([_dataList count] || index == 0){
                //已经加载过了
                [_dataTable reloadData];
            }else {
                NSDictionary *cate = [_categoryList objectAtIndex:index];
                NSString *cateId = [cate objectForKey:@"id"];
                [self fetchMenuListFromServer:cateId atCateIndex:index];
            }
        }
}

//===================================================================================_dataTable

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_dataList count];
}

- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 33;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (self.isOpen) {
        if (self.selectIndex.section == section) {
            return 2;
        }
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableDictionary *data = [_dataList objectAtIndex:indexPath.section];
    
    NSNumber *nowCount = [data objectForKey:@"count"];
    NSString *name = [data objectForKey:@"name"];
    NSNumber *price = [data objectForKey:@"price"];
    //NSNumber *id = [data objectForKey:@"id"];
    
    
    //返回子节点 视图
    if (self.isOpen && self.selectIndex.section == indexPath.section && indexPath.row!=0) {
    
        MenuUpdater *updater = [[[NSBundle mainBundle] loadNibNamed:@"MenuUpdater" owner:self options:nil] objectAtIndex:0];
        updater.frame = CGRectMake(0, 0, 320, 33);
        
        [updater.stepper setValue : [nowCount integerValue]];
        [updater.stepper setMaximumValue:100.0];
        
        //垃圾箱点击
        [updater.trash addTarget:self action:@selector(unOrderOne:) forControlEvents:UIControlEventTouchUpInside];
        //来一份
        [updater.stepper addTarget:self action:@selector(orderOne:) forControlEvents:UIControlEventValueChanged];
        
        [data setValue: nowCount forKey:@"count"];
        return updater;
    }else {
    
        MenuCell *menu = [[[NSBundle mainBundle] loadNibNamed:@"Menu" owner:self options:nil] objectAtIndex:0];
        menu.frame = CGRectMake(0, 0, 320, 33);
        //count
        if (nowCount && [nowCount intValue] > 0) {
            [menu.count setText: [NSString stringWithFormat:@"%d",[nowCount integerValue]]];
        }else {
            [menu.count setHidden:YES];
        }
        //name
        [menu.name setText:name];
        [menu.price setText:[NSString stringWithFormat:@"%.1f",[price doubleValue]]];
        
        
        
        return menu;
    }
}
//点击以后
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //row ==0 说明是选中了父节点
    if (indexPath.row == 0) {
        //已经备选中过了
        if ([indexPath isEqual:self.selectIndex]) {
            self.isOpen = NO;
            [self didSelectCellRowFirstDo:NO nextDo:NO];
            self.selectIndex = nil;
            //如果父节点没有备选中过，那么有两种情况，1他前面有被选中的父节点，2他前面没有被选中过的父节点
        }else
        {
            //首次加载，前面没有被选中过的父节点
            if (!self.selectIndex) {
                self.selectIndex = indexPath;
                [self didSelectCellRowFirstDo:YES nextDo:NO];
                
                //否则前面肯定有被选中过的父节点，那么把当前选中的父节点设置成不选中，同时把点击的节点设置成选中
            }else
            {
                
                [self didSelectCellRowFirstDo:NO nextDo:YES];
            }
        }
        
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



//父节点自身是否选中，另一个被选中的父节点是否选中
- (void)didSelectCellRowFirstDo:(BOOL)firstDoInsert nextDo:(BOOL)nextDoInsert
{
    self.isOpen = firstDoInsert;
    //找到已经打开的节点，收起父节点
    //Cell1 *cell = (Cell1 *)[_dataTable cellForRowAtIndexPath:self.selectIndex];
    //[cell changeArrowWithUp:firstDoInsert];
    
    //收起子节点
    [_dataTable beginUpdates];
    
    int section = self.selectIndex.section;
    
    NSIndexPath *indexPathToInsert = [NSIndexPath indexPathForRow:1 inSection:section];
    NSArray *rowToInsert = [[NSArray alloc] initWithObjects:indexPathToInsert, nil];
    
	
	if (firstDoInsert)
    {   [_dataTable insertRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
    }
	else
    {
        [_dataTable deleteRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
    }
	
	[_dataTable endUpdates];

    //打开下一个组
    if (nextDoInsert) {
        self.isOpen = YES;
        self.selectIndex = [_dataTable indexPathForSelectedRow];
        [self didSelectCellRowFirstDo:YES nextDo:NO];
    }
    if (self.isOpen) [_dataTable scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionTop animated:YES];
    [rowToInsert release];
}



//获取总的订单个数
-(NSInteger) getTotalCount{
    NSInteger count = 0;
    for (NSArray * menus in _dataAllList) {
        for (NSDictionary *menu in menus) {
            NSNumber *count_ = [menu valueForKey:@"count"];
            count = count + [count_ integerValue];
        }
    }
    return count;
}

//获取总的订单价格
-(double) getTotalPrice{
    NSInteger count = 0;
    double price = 0;
    for (NSArray * menus in _dataAllList) {
        
        for (NSDictionary *menu in menus) {
            NSNumber *count_ = [menu valueForKey:@"count"];
            NSNumber *price_ = [menu valueForKey:@"price"];
            count = count + [count_ integerValue];
            price = price + [price_ doubleValue] * [count_ doubleValue];
        }
    }
    return price;
}


//更新底部信息
-(void) updateBottomBar{
    
    NSInteger count = [self getTotalCount];
    double price = [self getTotalPrice];
    //总份数
    JSBadgeView *badgeView = [[JSBadgeView alloc] initWithParentView:allCount alignment:JSBadgeViewAlignmentCenter];
    if (count <10) {
        badgeView.badgeText = [NSString stringWithFormat:@"%@%d%@",@" ",count,@" "];
    }else{
        badgeView.badgeText = [NSString stringWithFormat:@"%d",count];
    }
    [badgeView release];
    [self.totalPrice setText: [NSString stringWithFormat:@"%.1f",price]];
    
    //配送费
    if (price >= _deliverPrice) {
        
        [self.order setTitle:@"下单" forState:UIControlStateNormal];
    }else {
        
        NSString *priceText = [NSString stringWithFormat:@"%@%.1f%@",@"还差",_deliverPrice - price,@"满起送价"];
        [self.order setTitle:priceText forState:UIControlStateNormal];
    }
}


//点击来一份
-(void) orderOne:(id)sender{
    UIStepper *st = (UIStepper *)sender;
    NSNumber *menuCount = [NSNumber numberWithFloat:st.value];

    NSDictionary *data = [_dataList objectAtIndex:selectIndex.section];
    [data setValue: menuCount forKey:@"count"];
    //添加已选菜单
    if (NO == [_orderList containsObject:data] && [menuCount integerValue] >0) {
        [_orderList addObject:data];
    }else if(YES == [_orderList containsObject:data] && [menuCount integerValue] == 0){
        [_orderList removeObject:data];
    }
    
    [_dataTable reloadData];
    [self updateBottomBar];
}
//点击垃圾箱
-(void) unOrderOne:(id)sender{

    NSDictionary *data = [_dataList objectAtIndex:selectIndex.section];
    [data setValue: 0 forKey:@"count"];
    [_dataTable reloadData];
    
    if ([_orderList containsObject:data]) {
        [_orderList removeObject:data];
    }
    [self updateBottomBar];
}

//==============POPList
-(void) initPopList:(id) sender{
    
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 310.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    _poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight -40)];
    _poplistview.delegate = self;
    _poplistview.datasource = self;
    _poplistview.listView.scrollEnabled = TRUE;
    [_poplistview setTitle:@"我的菜单"];
    
    
    UIButton *button_back = [[UIButton alloc] initWithFrame:CGRectMake(0, 230, xWidth/2-1, 40)];
    [button_back setTitle:@"返回" forState:UIControlStateNormal];
    button_back.backgroundColor = [UIColor orangeColor];
    
    [button_back addTarget:self action:@selector(getBack:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    UIButton *button_order = [[UIButton alloc] initWithFrame:CGRectMake(xWidth/2, 230, xWidth/2-1, 40)];
    [button_order setTitle:@"下单" forState:UIControlStateNormal];
    button_order.backgroundColor = [UIColor orangeColor];
    
    [_poplistview addSubview:button_back];
    [_poplistview addSubview:button_order];
    [_poplistview show];
    
    [_poplistview release];
    [button_back release];
    [button_order release];
}


#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath
{
    MenuCell_list *cell = [[[NSBundle mainBundle] loadNibNamed:@"MenuCell_list" owner:_poplistview options:nil] objectAtIndex:0];
    //最后一个就返回总价格
    if(indexPath.row == [_orderList count]){
        NSInteger totalCount = [self getTotalCount];
        double totalPrice = [self getTotalPrice];
        
        [cell.name setTextColor:[UIColor orangeColor]];
        [cell.name setText:@"总价格"];
        [cell.count setText:[NSString stringWithFormat:@"%d",totalCount]];
        [cell.price setText:[NSString stringWithFormat:@"%.1f",totalPrice]];
        return cell;
    }
    
    if(indexPath.row == [_orderList count]+1){
        for (UIView *v in cell.subviews){
            [v  removeFromSuperview];
        }
        return cell;
    }
    
    NSDictionary *menu = [_orderList objectAtIndex:indexPath.row];
    
    NSNumber *count = [menu objectForKey:@"count"];
    NSNumber *price = [menu objectForKey:@"price"];
    double totalPrice = [price floatValue] * [count integerValue];
    
    [cell.name setText:[menu objectForKey:@"name"]];
    [cell.count setText:[NSString stringWithFormat:@"%d",[count integerValue]]];
    [cell.price setText:[NSString stringWithFormat:@"%.1f",totalPrice]];
    
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section
{
    return [_orderList count]+2;
}

#pragma mark - UIPopoverListViewDelegate
- (void)popoverListView:(UIPopoverListView *)popoverListView
     didSelectIndexPath:(NSIndexPath *)indexPath
{
    // your code here
}

- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0f;
}
//返回
-(void) getBack:(id) sender{

    [_poplistview dismiss];
}

@end
