//
//  main.m
//  cook
//
//  Created by chenwenpeng on 13-12-28.
//  Copyright (c) 2013年 cook. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CAppDelegate class]));
    }
}
