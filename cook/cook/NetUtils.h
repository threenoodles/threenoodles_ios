//
//  NetUtils.h
//  cook
//
//  Created by chenwenpeng on 14-2-9.
//  Copyright (c) 2014年 cook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetUtils : NSObject

+(NSString*)currentNetworkType;
@end
