//
//  Utils.h
//  cook
//
//  Created by chenwenpeng on 14-2-22.
//  Copyright (c) 2014年 cook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

//按照string 解析数字，如果异常，返回 -1
+(NSInteger) parseInteger:(NSString*) number;

+(NSInteger) parseDouble:(NSString*) number;

//判断电话号码
+(BOOL) isMobileNumber:(NSString*) number;
@end
