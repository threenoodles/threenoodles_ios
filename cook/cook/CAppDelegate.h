//
//  CAppDelegate.h
//  cook
//
//  Created by chenwenpeng on 13-12-28.
//  Copyright (c) 2013年 cook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
