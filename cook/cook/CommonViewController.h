//
//  CommonViewController.h
//  cook
//
//  Created by chenwenpeng on 14-2-17.
//  Copyright (c) 2014年 cook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sqlite3.h"

#define DATA_FILE @"cook_db_name"
#define TABLE_NAME @"t_address"

#define SUCCESS_LOGIN @"success"
@interface CommonViewController : UIViewController
{
        sqlite3 *db;
        NSMutableDictionary *_address;
}

-(void) createDB;

-(void) insert:(NSString *) buildingId buildingName:(NSString *) buildingName addresName:(NSString *) addressName phone:(NSString *) phone userId:(NSString *) userId userName:(NSString *)userName;

- (NSMutableDictionary *) query;

-(void) update:(NSString *) buildingId buildingName:(NSString *)buildingName;

-(void) update:(NSString *) userId userName:(NSString *)userName;

-(void) dropData;
@end
