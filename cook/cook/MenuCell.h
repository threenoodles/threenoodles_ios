//
//  MenuCell.h
//  cook
//
//  Created by chenwenpeng on 14-1-12.
//  Copyright (c) 2014年 cook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell

@property (nonatomic,retain)IBOutlet UILabel *name;
@property (nonatomic,retain)IBOutlet UILabel *price;
@property (nonatomic,retain)IBOutlet UILabel *count;

@end
