//
//  MenuCell_list.m
//  cook
//
//  Created by chenwenpeng on 14-1-19.
//  Copyright (c) 2014年 cook. All rights reserved.
//

#import "MenuCell_list.h"

@implementation MenuCell_list

@synthesize name;
@synthesize count;
@synthesize price;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
