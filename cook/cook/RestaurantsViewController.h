//
//  RestaurantsViewController.h
//  cook
//
//  Created by chenwenpeng on 13-12-29.
//  Copyright (c) 2013年 cook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"

#define NO_BUILDING @"-1"
@interface RestaurantsViewController : CommonViewController

@property (nonatomic,assign) NSString *buildingId;
@property (nonatomic,assign) NSString *buildingName;


@end
