//
//  RegistController.h
//  cook
//
//  Created by chenwenpeng on 14-2-6.
//  Copyright (c) 2014年 cook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Toast+UIView.h"
#import "HttpPostExecutor.h"
#import "CommonViewController.h"
#import "Utils.h"


#define FLAG_AGREEMENT @"true"
#define FLAG_UNAGREEMENT @"false"

#define FLAG_FAILD_USERNAMEUSED @"fail_usernameUsed"
#define FLAG_FAILD_PHONEUSED @"fail_phoneUsed"
#define FLAG_SUCCESS @"success"

#define TO_MYCENTER @"toMycenter"
#define TO_CONFIRMORDER @"toConfirmOrder"

#define TAG_XIB_USERNAME -7
#define TAG_XIB_PHONE -8
#define TAG_XIB_PASSWORD -9
#define TAG_XIB_REPASSWORD -10
#define TAG_XIB_SWITCH -11
#define TAG_XIB_AGREEMENT -12
#define TAG_XIB_REGIST -13
@interface RegistController : CommonViewController<UITextFieldDelegate>

@property (nonatomic,retain) IBOutlet UITextField *username;

@property (nonatomic,retain) IBOutlet UITextField *phone;

@property (nonatomic,retain) IBOutlet UITextField *password;

@property (nonatomic,retain) IBOutlet UITextField *rePassword;

@property (nonatomic,retain) NSString *toWhere;


@end
