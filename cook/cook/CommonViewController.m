//
//  CommonViewController.m
//  cook
//
//  Created by chenwenpeng on 14-2-17.
//  Copyright (c) 2014年 cook. All rights reserved.
//

#import "CommonViewController.h"

@interface CommonViewController ()

@end

@implementation CommonViewController

- (void)dealloc
{
    [super dealloc];
    [_address release];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _address = [[NSMutableDictionary alloc]init];
    NSLog(@"super view did load~");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//===db
-(NSString *)dataFilePath {
    NSArray * myPaths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * myDocPath = [myPaths objectAtIndex:0];
    NSString *filename = [myDocPath stringByAppendingPathComponent:DATA_FILE];
    return filename;
}

-(void) createDB{
    NSString *path = [self dataFilePath];
    int n = sqlite3_open([path UTF8String], &db);
    if(SQLITE_OK == n){
        char *err;
		NSString *createSQL = @"create table if not exists mf_contact (building_id text,building_name text,address text,phone text,user_id text,user_name text)";
		if (sqlite3_exec(db,[createSQL UTF8String],NULL,NULL,&err) == SQLITE_OK) {
            NSMutableDictionary *address = [self query];
            if (address && [address count]<=0) {
                NSLog(@"初始化数据库数据成功!");
                [self insert:@"-1" buildingName:@"" addresName:@"" phone:@"" userId:@"-1" userName:@""];
            }
            
		}
    }
}

-(void) insert:(NSString *) buildingId buildingName:(NSString *) buildingName addresName:(NSString *) addressName phone:(NSString *) phone userId:(NSString *) userId userName:(NSString *) userName{
    
    NSString *filename = [self dataFilePath];
	if (sqlite3_open([filename UTF8String], &db) == SQLITE_OK) {
        
        NSString *sqlStr = @"insert or replace into mf_contact values (?,?,?,?,?,?)";
		sqlite3_stmt *statement;
        
		//预处理过程
		if (sqlite3_prepare_v2(db, [sqlStr UTF8String], -1, &statement, NULL) == SQLITE_OK) {
			//绑定参数开始
            sqlite3_bind_text(statement,1,[buildingId UTF8String],-1,NULL);
			sqlite3_bind_text(statement, 2, [buildingName UTF8String], -1, NULL);
			sqlite3_bind_text(statement, 3, [addressName UTF8String], -1, NULL);
			sqlite3_bind_text(statement, 4, [phone UTF8String], -1, NULL);
			sqlite3_bind_text(statement,5,[userId UTF8String],-1,NULL);
            sqlite3_bind_text(statement,6,[userName UTF8String],-1,NULL);
            
			sqlite3_step(statement);
		}
		
		sqlite3_finalize(statement);
		sqlite3_close(db);
		
	}
}


- (NSMutableDictionary *) query{
    
	NSString *filename = [self dataFilePath];
	if (sqlite3_open([filename UTF8String], &db) == SQLITE_OK) {
		NSString *qsql = @"select building_id,building_name,address,phone,user_id,user_name from mf_contact limit 1";
		sqlite3_stmt *statement;
		//预处理过程
		if (sqlite3_prepare_v2(db, [qsql UTF8String], -1, &statement, NULL) == SQLITE_OK) {
			//执行
			while (sqlite3_step(statement) == SQLITE_ROW) {
				
				char *f1 = (char *) sqlite3_column_text(statement, 0);
				NSString *buildingId = [[NSString alloc] initWithUTF8String: f1];
				char *f2 = (char *) sqlite3_column_text(statement, 1);
				NSString *buildingName = [[NSString alloc] initWithUTF8String: f2];
                char *f3 = (char *) sqlite3_column_text(statement, 2);
				NSString *address = [[NSString alloc] initWithUTF8String: f3];
				char *f4 = (char *) sqlite3_column_text(statement, 3);
				NSString *phone = [[NSString alloc] initWithUTF8String: f4];
                char *f5 = (char *) sqlite3_column_text(statement, 4);
                
				NSString *userId = [[NSString alloc] initWithUTF8String: f5 == nil? "-1":f5];
				char *f6 = (char *) sqlite3_column_text(statement, 5);
				NSString *userName = [[NSString alloc] initWithUTF8String: f6 == nil ? "" : f6];
                
                
                [_address setObject:buildingId forKey:@"buildingId"];
                [_address setObject:buildingName forKey:@"buildingName"];
                [_address setObject:address forKey:@"address"];
                [_address setObject:phone forKey:@"phone"];
                [_address setObject:userId forKey:@"userId"];
                [_address setObject:userName forKey:@"userName"];
                
                
                [buildingId release];
                [buildingName release];
                [address release];
                [phone release];
                [userId release];
                [userName release];
			}
		}
		sqlite3_finalize(statement);
		sqlite3_close(db);
		
	}
    return _address;
}


-(void) update:(NSString *) buildingId buildingName:(NSString *)buildingName{
    
    NSString *filename = [self dataFilePath];
	if (sqlite3_open([filename UTF8String], &db) == SQLITE_OK) {
        
        NSString *updateSql = @"update mf_contact set building_id = ?,building_name = ?";
		sqlite3_stmt *statement;
		//预处理过程
		if (sqlite3_prepare_v2(db, [updateSql UTF8String], -1, &statement, NULL) == SQLITE_OK) {
			//绑定参数开始
            sqlite3_bind_text(statement,1,[buildingId UTF8String],-1, NULL);
			sqlite3_bind_text(statement, 2, [buildingName UTF8String], -1, NULL);
			
			sqlite3_step(statement);
		}
		
		sqlite3_finalize(statement);
		sqlite3_close(db);
		
	}
}


-(void) update:(NSString *) userId userName:(NSString *)userName{
    
    NSString *filename = [self dataFilePath];
	if (sqlite3_open([filename UTF8String], &db) == SQLITE_OK) {
        
        NSString *updateSql = @"update mf_contact set user_id = ?,user_name = ?";
		sqlite3_stmt *statement;
		//预处理过程
		if (sqlite3_prepare_v2(db, [updateSql UTF8String], -1, &statement, NULL) == SQLITE_OK) {
			//绑定参数开始
            sqlite3_bind_text(statement,1,[userId UTF8String],-1, NULL);
			sqlite3_bind_text(statement, 2, [userName UTF8String], -1, NULL);
			
			sqlite3_step(statement);
		}
		
		sqlite3_finalize(statement);
		sqlite3_close(db);
		
	}
}




-(void) dropData{
    char *err;
    NSString *deleteSql = @"delete from mf_contact";
    sqlite3_exec(db, [deleteSql UTF8String], NULL, NULL, &err);
    sqlite3_close(db);
}
@end
