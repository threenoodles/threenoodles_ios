//
//  ViewController.h
//  Foodspotting
//
//  Created by jetson  on 12-8-15.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
@interface MenuViewController : UIViewController<UIScrollViewDelegate,iCarouselDataSource, iCarouselDelegate>{
    UIScrollView *homeTopScrollView;
    
}

@property (retain, nonatomic) NSString *buildingId;

@property (retain, nonatomic) NSString *restId;

@property (retain, nonatomic) NSString *restName;

@property (retain, nonatomic) IBOutlet iCarousel *carousel;

@property (retain, nonatomic) IBOutlet UIImageView *homeTopSVBack;

@property (retain,nonatomic) IBOutlet UILabel *allCount;

@property (retain,nonatomic) IBOutlet UILabel *totalPrice;

@property (retain,nonatomic) IBOutlet UIButton *order;

@property (retain,nonatomic) IBOutlet UIButton *cart;
@end
