//
//  MenuCell_list.h
//  cook
//
//  Created by chenwenpeng on 14-1-19.
//  Copyright (c) 2014年 cook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell_list : UITableViewCell

@property (nonatomic,retain) IBOutlet UILabel *name;
@property (nonatomic,retain) IBOutlet UILabel *count;
@property (nonatomic,retain) IBOutlet UILabel *price;

@end
