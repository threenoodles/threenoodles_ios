//
//  RegistController.m
//  cook
//
//  Created by chenwenpeng on 14-2-6.
//  Copyright (c) 2014年 cook. All rights reserved.
//

#import "RegistController.h"

@interface RegistController (){

    UITextField *_usernameField;
    UITextField *_phoneField;
    UITextField *_passwordField;
    UITextField *_repasswordField;
    UISwitch *_switchAgreement;
    NSString *toWhere;
}

@end

@implementation RegistController

@synthesize username;
@synthesize phone;
@synthesize password;
@synthesize rePassword;

@synthesize toWhere;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


//转到登录
-(void) toLogin:(id *) sender{

    [self dismissModalViewControllerAnimated:YES];
}

//注册
-(void) regist:(id *) sender{
    
    if([_switchAgreement isOn]){
        NSString *usernameText = [[self username] text];
        NSString *phoneText = [[self phone] text];
        NSString *passwordText = [[self password] text];
        NSString *repasswordText = [[self rePassword] text];
        
        if(usernameText == nil || [usernameText isEqualToString:@""]){
            
            [self.view makeToast:@"用户名不能为空" duration:1.0 position:@"center"];
            return;
        }
        if([usernameText length] <3 || [usernameText length]>20){
            
            [self.view makeToast:@"用户名请在3-20个字符之间" duration:1.0 position:@"center"];
            return;
        }
        
        if(phoneText == nil || [phoneText isEqualToString:@""]){
         
            [self.view makeToast:@"收餐电话不能为空" duration:1.0 position:@"center"];
            return;
        }
        
        if(![Utils isMobileNumber:phoneText]){
            
            [self.view makeToast:@"请正确填写收餐电话" duration:1.0 position:@"center"];
            return;
        }
        
        if(passwordText == nil || [passwordText isEqualToString:@""]){
            
            [self.view makeToast:@"密码不能为空" duration:1.0 position:@"center"];
            return;
        }
        if(repasswordText == nil || [repasswordText isEqualToString:@""]){
            [self.view makeToast:@"密码不能为空" duration:1.0 position:@"center"];
            return;
        }
        
        if(![repasswordText isEqualToString:passwordText]){
            
            [self.view makeToast:@"两次密码不相同" duration:1.0 position:@"center"];
            return;
        }
        
        NSString * encodedUserName = (NSString *)CFURLCreateStringByAddingPercentEscapes( kCFAllocatorDefault, (CFStringRef)usernameText, NULL, NULL,  kCFStringEncodingUTF8 );
        //注册测试
        NSString *url = [NSString stringWithFormat:@"%@%@%@%@%@%@",@"http://www.0707wm.com/android/regist?username=",encodedUserName,@"&phone=",phoneText,@"&password=",passwordText];
        
        [HttpPostExecutor postExecuteWithUrlStr: url
                                      Paramters:@""
                            FinishCallbackBlock:^(NSString *resultStr){
                                if(resultStr == nil || [resultStr isEqualToString:@""]){
                                    resultStr = @"";
                                }
                                //执行post请求完成后的逻辑
                                NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData: [resultStr dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
                                
                                NSString *flag = [resultDic objectForKey:@"flag"];
                                
                                if([FLAG_SUCCESS isEqualToString:flag]){
                                    
                                    [self.view makeToast:@"恭喜您，注册成功" duration:1.0 position:@"center"];
                                    NSString *userid = [NSString stringWithFormat:@"%@",[resultDic objectForKey:@"id"]];
                                    //存库
                                    [self update:userid userName:usernameText];
                                    //跳转都登录页
                                    if([self toWhere] == nil || [[self toWhere] isEqualToString:TO_MYCENTER]){
                                        
                                        [self toLogin:nil];
                                    }else if([[self toWhere] isEqualToString:TO_CONFIRMORDER]){
                                        
                                    }
                                    
                                }else if([FLAG_FAILD_USERNAMEUSED isEqualToString:flag]){
                                    
                                    [self.view makeToast:@"对不起，用户名已经被注册" duration:1.0 position:@"center"];
                                }else if([FLAG_FAILD_PHONEUSED isEqualToString:flag]){
                                    
                                    [self.view makeToast:@"对不起，电话已经被使用" duration:1.0 position:@"center"];
                                }else{
                                    
                                    [self.view makeToast:@"服务器正忙，请稍后重试" duration:1.0 position:@"center"];
                                }
                                
                                
                            } ErrorCallbackBlock:^(NSString *error){
                                
                                [self.view makeToast:@"网络不给力" duration:1.0 position:@"center"];
                            }];
        
        
    }else{
     
        [self.view makeToast:@"请您同意服务协议" duration:1.0 position:@"center"];
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    username.delegate = self;
    phone.delegate = self;
    password.delegate = self;
    rePassword.delegate = self;
    
    //导航
    UILabel *titleView = [[[NSBundle mainBundle] loadNibNamed:@"TitleView" owner:self options:nil] objectAtIndex:0];
    titleView.frame = CGRectMake(70, 0, 180, 40);
    [titleView setTextAlignment:NSTextAlignmentCenter];
    [titleView setText:@"注册"];
    
    UIButton *left = [[[NSBundle mainBundle] loadNibNamed:@"TitleTextView" owner:self options:nil] objectAtIndex:0];
    [left setFrame:CGRectMake(0, 0, 70, 40)];
    [self.view addSubview:left];
    
    UIButton *right = [[[NSBundle mainBundle] loadNibNamed:@"TitleTextView" owner:self options:nil] objectAtIndex:0];
    right.frame = CGRectMake(250, 0, 70, 40);
    [right setTitle:@"登录" forState:UIControlStateNormal];
    [right addTarget:self action:@selector(toLogin:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:right];
    [self.view addSubview:titleView];
    
    _switchAgreement = (UISwitch *)[self.view viewWithTag:TAG_XIB_SWITCH];
    [_switchAgreement setOn:true];
    
    
    //注册
    UIButton *registButton = (UIButton *)[self.view viewWithTag:TAG_XIB_REGIST];
    [registButton addTarget: self action:@selector(regist:) forControlEvents:UIControlEventTouchUpInside];
    
    self.view.backgroundColor = [UIColor colorWithRed:243/255.0 green:243/255.0 blue:243/255.0 alpha:1];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    //返回一个BOOL值，指明是否允许在按下回车键时结束编辑
    if(textField.tag == TAG_XIB_REPASSWORD){
        [self regist:nil];
    }else {
        [textField resignFirstResponder];
    }
    return YES;
}
@end
