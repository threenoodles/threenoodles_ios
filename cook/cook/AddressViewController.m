//
//  DiYuListViewController.m
//  IYLM
//
//  Created by Jian-Ye on 12-10-30.
//  Copyright (c) 2012年 Jian-Ye. All rights reserved.
//

#import "AddressViewController.h"
#import "Cell1.h"
#import "Cell2.h"
#import "RestaurantsViewController.h"
#import "NetUtils.h"
#import "Toast+UIView.h"
#import "HttpPostExecutor.h"

@interface AddressViewController()<UITableViewDataSource,UITableViewDelegate>{
@private
    NSMutableArray *_dataList;
@private
    UITableView *_dataTable;
}
@property (assign)BOOL isOpen;
@property (nonatomic,retain)NSIndexPath *selectIndex;

@property (nonatomic,retain) UITableView *expansionTableView;
@end

@implementation AddressViewController

@synthesize isOpen,selectIndex;
@synthesize expansionTableView;

- (void)dealloc
{
    [_dataTable release];
    [_dataList release];
    _dataList = nil;
    _dataTable = nil;
    self.isOpen = NO;
    self.selectIndex = nil;
    [super dealloc];
    
}

-(void) fetchData:(id)sender{
    
    [HttpPostExecutor postExecuteWithUrlStr:@"http://www.0707wm.com/android/getAddresses"
                                  Paramters:@""
                        FinishCallbackBlock:^(NSString *result){
                            
                            if(result == nil || [result isEqualToString:@""]){
                                result = @"";
                            }
                            
                            // 执行post请求完成后的逻辑
                            NSMutableArray *regionList = [NSJSONSerialization JSONObjectWithData: [result dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
                            //set一遍才能起作用
                            [_dataList removeAllObjects];
                            [_dataList addObjectsFromArray:regionList];
                            [_dataTable reloadData];
                        } ErrorCallbackBlock:^(NSString *error){
                            [self.view makeToast:@"网络不给力" duration:1.0 position:@"center"];
                        }];
    
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _dataList = [[NSMutableArray alloc] init];
    _address = [[NSMutableDictionary alloc] init];
    //获取数据
    [self fetchData:nil];
    
    UILabel *titleView = [[[NSBundle mainBundle] loadNibNamed:@"TitleView" owner:self options:nil] objectAtIndex:0];
    titleView.frame = CGRectMake(70, 0, 180, 40);
    [titleView setTextAlignment:NSTextAlignmentCenter];
    [titleView setText:@"地址"];
    
    UIButton *left = [[[NSBundle mainBundle] loadNibNamed:@"TitleTextView" owner:self options:nil] objectAtIndex:0];
    [left setFrame:CGRectMake(0, 0, 70, 40)];

    [self.view addSubview:left];
    
    UIButton *right = [[[NSBundle mainBundle] loadNibNamed:@"TitleImageView" owner:self options:nil] objectAtIndex:0];
    right.frame = CGRectMake(250, 0, 70, 40);
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(30, 10, 25, 20)];
    [image setImage:[UIImage imageNamed:@"rightRefresh.png"]];
    [right addSubview:image];
    [self.view addSubview:right];
    [right addTarget: self action:@selector(fetchData:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:titleView];
    
    
    _dataTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 40, 320, 420)];
    [_dataTable setDelegate:self];
    [_dataTable setDataSource:self];
    [self.view addSubview:_dataTable];
    
    _dataTable.sectionFooterHeight = 0;
    _dataTable.sectionHeaderHeight = 0;
    self.isOpen = NO;
    
    [image release];
}

- (void)timerFireMethod:(NSTimer*)theTimer
{
    UIAlertView *promptAlert = (UIAlertView*)[theTimer userInfo];
    [promptAlert dismissWithClickedButtonIndex:0 animated:NO];
    
    [promptAlert release];
    promptAlert = NULL;
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_dataList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isOpen) {
        if (self.selectIndex.section == section) {
            int count = [[[_dataList objectAtIndex:section] objectForKey:@"buildingList"] count]+1;
            return count;
        }
    }
    return 1;
}
- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //更新子节点
    if (self.isOpen && self.selectIndex.section == indexPath.section && indexPath.row!=0) {
        static NSString *CellIdentifier = @"Cell2";
        Cell2 *cell = (Cell2*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
        }
        NSArray *list = [[_dataList objectAtIndex:self.selectIndex.section] objectForKey:@"buildingList"];
        cell.titleLabel.text = [[list objectAtIndex:indexPath.row-1] objectForKey:@"name"];
        return cell;
        
        //更新父节点
    }else
    {
        static NSString *CellIdentifier = @"Cell1";
        Cell1 *cell = (Cell1*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil] objectAtIndex:0];
        }
        NSString *name = [[_dataList objectAtIndex:indexPath.section] objectForKey:@"name"];
        cell.titleLabel.text = name;
        [cell changeArrowWithUp:([self.selectIndex isEqual:indexPath]?YES:NO)];
        return cell;
    }
}


#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //row ==0 说明是选中了父节点
    if (indexPath.row == 0) {
        //已经备选中过了
        if ([indexPath isEqual:self.selectIndex]) {
            self.isOpen = NO;
            [self didSelectCellRowFirstDo:NO nextDo:NO];
            self.selectIndex = nil;
            //如果父节点没有备选中过，那么有两种情况，1他前面有被选中的父节点，2他前面没有被选中过的父节点
        }else
        {
            //首次加载，前面没有被选中过的父节点
            if (!self.selectIndex) {
                self.selectIndex = indexPath;
                [self didSelectCellRowFirstDo:YES nextDo:NO];
                
                //否则前面肯定有被选中过的父节点，那么把当前选中的父节点设置成不选中，同时把点击的节点设置成选中
            }else
            {
                
                [self didSelectCellRowFirstDo:NO nextDo:YES];
            }
        }
        
    }else{

        //NSDictionary *building = [[_dataList objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        NSDictionary *region = [_dataList objectAtIndex:indexPath.section];
        NSArray *buildingList = [region objectForKey:@"buildingList"];
        NSDictionary *building = [buildingList objectAtIndex:indexPath.row-1];
        
        //更新数据库
        NSString *buildingId = [NSString stringWithFormat:@"%@",[building objectForKey:@"id"]];
        NSString *buildingName = [building objectForKey:@"name"];
        [self update:buildingId buildingName:buildingName];
        
        [self dismissModalViewControllerAnimated:YES];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

//父节点自身是否选中，另一个被选中的父节点是否选中
- (void)didSelectCellRowFirstDo:(BOOL)firstDoInsert nextDo:(BOOL)nextDoInsert
{
    self.isOpen = firstDoInsert;
    //找到已经打开的节点，收起父节点
    Cell1 *cell = (Cell1 *)[_dataTable cellForRowAtIndexPath:self.selectIndex];
    [cell changeArrowWithUp:firstDoInsert];
    
    //收起子节点
    [_dataTable beginUpdates];
    
    int section = self.selectIndex.section;
    int contentCount = [[[_dataList objectAtIndex:section] objectForKey:@"buildingList"] count];
	NSMutableArray* rowToInsert = [[NSMutableArray alloc] init];
	for (NSUInteger i = 1; i < contentCount + 1; i++) {
		NSIndexPath* indexPathToInsert = [NSIndexPath indexPathForRow:i inSection:section];
		[rowToInsert addObject:indexPathToInsert];
	}
	
	if (firstDoInsert)
    {   [_dataTable insertRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
    }
	else
    {
        [_dataTable deleteRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
    }
    
	[rowToInsert release];
	
	[_dataTable endUpdates];
    //打开下一个组
    if (nextDoInsert) {
        self.isOpen = YES;
        self.selectIndex = [_dataTable indexPathForSelectedRow];
        [self didSelectCellRowFirstDo:YES nextDo:NO];
    }
    if (self.isOpen) [_dataTable scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionTop animated:YES];
}


@end
