//
//  MenuUpdater.m
//  cook
//
//  Created by chenwenpeng on 14-1-12.
//  Copyright (c) 2014年 cook. All rights reserved.
//

#import "MenuUpdater.h"

@implementation MenuUpdater
@synthesize trash;
@synthesize stepper;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) dealloc{

    self.trash = nil;
    self.stepper = nil;
    
    [super dealloc];
}
@end
