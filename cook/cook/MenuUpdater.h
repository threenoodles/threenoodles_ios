//
//  MenuUpdater.h
//  cook
//
//  Created by chenwenpeng on 14-1-12.
//  Copyright (c) 2014年 cook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuUpdater : UITableViewCell

@property (nonatomic,retain) IBOutlet UIButton *trash;
@property (nonatomic,retain) IBOutlet UIStepper *stepper;
@end
