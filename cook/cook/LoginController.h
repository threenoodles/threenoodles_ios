//
//  LoginController.h
//  cook
//
//  Created by chenwenpeng on 14-1-19.
//  Copyright (c) 2014年 cook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"
#import "Toast+UIView.h"
#import "HttpPostExecutor.h"
#import "UserCenterView.h"
#import "RegistController.h"

#define NO_USER @"-1"
#define TO_MYCENTER @"toMyCenter"

#define TAG_USERNAME 1
#define TAG_PASSWORD 2

#define TAG_XIB_USERNAME -1
#define TAG_XIB_PHONE -2
#define TAG_XIB_ADDRESS -3

#define TAG_XIB_LOGOUT -4
#define TAG_XIB_CHANGEPHONE -5
#define TAG_XIB_CHANGEADDRESS -6

@interface LoginController : CommonViewController<UITextFieldDelegate>{
    @private
    NSString *flag;
}
@property (nonatomic,assign) NSString *flag;
@end
